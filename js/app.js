const valueTake = document.getElementById("searchUser");
const batonSearch = document.getElementById("batonSearch");
const kontenerName = document.getElementById("kontenerNameId");
const kontenerLogin = document.getElementById("kontenerLoginId");
const kontenerRepos = document.getElementById("kontenerReposId");
const kontenerUrl = document.getElementById("kontenerUrlId");
const writeName = document.getElementById("writeNameId");

const developer_github_id = "DEVELOPER-ID";
const developer_github_token = "DEVELOPER-TOKEN";
const url_git_user = "https://api.github.com/users";
const url_git_user_go = "https://github.com";

const fetchUsers = async (user) => {

    const api_call = await fetch(`${url_git_user}/${user}?developer_github_id=${developer_github_id}&developer_github_token=${developer_github_token}`);
    const api_call_language = await fetch(`${url_git_user}/${user}/repos?developer_github_id=${developer_github_id}&developer_github_token=${developer_github_token}`);

    const data = await api_call.json();
    const dane = await api_call_language.json();

    // zawżdy {dane: dane}
    return {data, dane }
};

const showData = () => {
    fetchUsers(valueTake.value).then((res) => {

        if(res.data.login != undefined && (valueTake.value.toLowerCase() == res.data.login.toLowerCase())){
          kontenerName.innerHTML = `Name: <span class="kontenerInfoUser">${res.data.name}</span>`;
          kontenerLogin.innerHTML = `Login: <span class="kontenerInfoUser">${res.data.login}</span>`;
          kontenerUrl.innerHTML = `More about  <span class="kontenerInfoUser">${res.data.login}: <a href="${url_git_user_go}/${res.data.login}">${url_git_user_go}/${res.data.login}</a></span>`;

          var tabelaJezyki = [];

          for(let i=0; i<res.dane.length; i++){
              if(res.dane[i].language != null){
                  tabelaJezyki.push(res.dane[i].language);
              }
          }

          let unique = [...new Set(tabelaJezyki)];

          kontenerRepos.innerHTML = `Programming languages: <span class="kontenerInfoUser">${unique.join(' ')} </span> `;
          valueTake.placeholder = "Again? Enter a username...";
        } else {
          clearInfos();
          writeName.innerHTML = "No User - try another name";
          writeName.style.color = "red";
          valueTake.placeholder = "Again? Enter a username...";
        }

    })
}

function clearInfos(){
  kontenerName.innerHTML = "";
  kontenerLogin.innerHTML = "";
  kontenerRepos.innerHTML = "";
  kontenerUrl.innerHTML = "";
}

batonSearch.addEventListener("click", () => {
    if(valueTake.value == "") {
      clearInfos();
      writeName.innerHTML = "Write name of github user";
      writeName.style.color = "#972B35";

    } else {
      writeName.innerHTML = "";

      showData();
    }
    valueTake.placeholder = "Again? Enter a username...";
})
